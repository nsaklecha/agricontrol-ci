export const environment = {
  production: true,
  apiBaseUrl: 'https://int.agricontrol.app/dashboard/index.php/',
  apiSiteUrl: 'https://int.agricontrol.app/dashboard/',
};
