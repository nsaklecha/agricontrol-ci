import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private subject = new Subject<any>();

  constructor() { }

  confirmThis(heading: string, message: string, siFn: () => void, noFn: () => void) {
    this.setConfirmation(heading, message, siFn, noFn);
  }

  setConfirmation(heading: string, message: string, siFn: () => void, noFn: () => void) {
    const that = this;
    this.subject.next({ type: 'confirm',
        headtext: heading,
        text: message,
        siFn:
        function() {
            that.subject.next();
            siFn();
        },
        noFn: function() {
            that.subject.next();
            noFn();
        }
    });
  }

  getMessage(): Observable<any> {
     return this.subject.asObservable();
  }
}
