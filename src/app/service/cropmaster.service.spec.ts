import { TestBed } from '@angular/core/testing';

import { CropmasterService } from './cropmaster.service';

describe('CropmasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CropmasterService = TestBed.get(CropmasterService);
    expect(service).toBeTruthy();
  });
});
