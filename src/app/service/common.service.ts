import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalstorageService } from './localstorage.service';
import { environment } from '../../environments/environment';
import { Profile } from '../model/profile';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  apiBaseUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient, private storage: LocalstorageService) { }

  getAllSubscriptions() {
    return this.http.get(this.apiBaseUrl + 'common/getAllSubscriptions');
  }

  getAllFaq(array) {
    //const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'common/getAllFaq',array);
  }

  getAllLanguages() {
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'common/getAllLanguage', authHeader);
  }

  getUserData() {
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'auth/getUserData', authHeader);
  }

  updateSuggestionStatus(data) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'auth/updatePopupStatus', data, authHeader);
  }
  
  getServerDate() {
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'common/getServerDate',authHeader);
  }

  upgradeUserPlan(planDetails: any) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'common/upgradeUserPlan', planDetails, authHeader);
  }

  sendAdminNotification(userID: any) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'auth/sendAdminNotification', userID, authHeader);
  }

  allLanguages() {
    return this.http.get(this.apiBaseUrl + 'common/getAllLanguage');
  }

  getHelpText(key: string) {
    const text = [];
    text['dashboard'] = [
      'Total Size Box Shows Tatal Area of all Fields.',
      'Number of Fields Box Shows count of Fields.',
      'Swapped Fields Box Shows count of Fields Swapped by other Farmers.',
      'Footer shows Total Cultivated Area and count of fields and swapped fields count and crop wise cultivation area of all field.'
    ];
    text['chart'] = [
      'Each row represent a field.',
      'Colored area over field row show that over that duration that particular crop is cultivated.',
      'Crops with color shows on header of chart to identify on chart.',
      'Month are shows on the field row to identify the duration of culture on field.',
      'On mouse hover on field row it shows the information of culture.',
      'For adding new culture on field each field has own button on side of field row of chart.',
      'For adding new field a button is given on the top of chart.',
      'There are three button or icons over each field row.',
      'Those button or icons are for editing field detail and to switch on detailed view of field and for archieve field.',
      'A time range slider is placed on the bottom of chart which is used for changing time range of chart.',
    ];
    text['agricontrol'] = [
      'The directive defines the maximum number of main crops within the from seven years ago.',
      ' Note: This means that the same main crops can be cultivated twice in a row. and then the cultivation breaks are combined.',
      'Only one main culture of the same family is allowed per year.',
      'The main cultures are cultures lasting more than 14 weeks.',
      'Short crops of 14 weeks or less shall be used for crop rotation according to OILN only relevant if, in the same year, two or more short crops of the same family can be cultivated.',
      'In this case, the combination of the same species counts as a main culture of the corresponding species',
      'or the combination of different species as one Main culture of the family.',
      'The main crops of the same family belong together to the maximum occupancy of the families within seven years.',
      'The values shown in the table for the individual families are also included in the maximum number of arable crops listed.',
      'In addition, the maximum occupancy of the individual type must be observed.',
      'If, after a main crop in the following year on the same parcel, the the same main crop is cultivated, then for main crops of the same family a break in cultivation of at least 2 years.',
      'The follower report shall be used in the control for the last seven years.'
    ];
    text['activities'] = [
      'From Settings current reporting year for report can be set or update.',
      'From crop master color of crops can be changed.',
      'checked crops in dropdown only used for filtering and maximum 5 crops can be placed for filter.'
    ];
    return text[key];
  }

  

}
