import { Component, OnInit } from '@angular/core';
import { AdminauthService } from '../service/adminauth.service';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.scss']
})
export class AdmindashboardComponent implements OnInit {

  allUsers$: any;

  constructor(private adminservice: AdminauthService) { }

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers() {
    this.adminservice.getAllUsers().subscribe(
      data => this.allUsers$ = data['data']
    );
  }

}
