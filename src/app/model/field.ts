export class Field {
    id: number;
    name: string;
    size: number;
    echo: boolean;
    echoSize: number;
    address: string;
    city: string;
    zipcode: string;
    notes: string;
}
