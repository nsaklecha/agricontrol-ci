import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IconEdit,
  IconChevronLeft,
  IconArchive,
  IconInfo,
  IconPlusCircle,
  IconCircle,
  IconX,
  IconAlertTriangle,
  IconRefreshCw,
  IconDownload,
  IconTrash2,
  IconRefreshCcw
} from 'angular-feather';

const icons = [
  IconEdit,
  IconChevronLeft,
  IconArchive,
  IconInfo,
  IconPlusCircle,
  IconCircle,
  IconX,
  IconAlertTriangle,
  IconRefreshCw,
  IconDownload,
  IconTrash2,
  IconRefreshCcw
];

@NgModule({
  imports: [
    CommonModule,
    icons
  ],
  exports: icons,
  declarations: []
})
export class IconsModule { }
