import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguagefileComponent } from './languagefile.component';

describe('LanguagefileComponent', () => {
  let component: LanguagefileComponent;
  let fixture: ComponentFixture<LanguagefileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanguagefileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguagefileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
